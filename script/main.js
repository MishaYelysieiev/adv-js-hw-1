
Hamburger.SIZE_SMALL = {
    type:'small',
    calories: 20,
    price: 50
};

Hamburger.SIZE_LARGE = {
    type:'large',
    calories: 40,
    price: 100
};

Hamburger.STUFFING_CHEESE = {
    type:'cheese',
    calories: 10,
    price: 20
};

Hamburger.STUFFING_SALAD = {
    type:'salad',
    calories: 5,
    price: 20
};

Hamburger.STUFFING_POTATO = {
    type:'potato',
    calories: 10,
    price: 15
};

Hamburger.TOPPING_MAYO = {
    type:'mayo',
    calories: 5,
    price: 20
};

Hamburger.TOPPING_SPICE = {
    type:'spice',
    calories: 0,
    price: 15
};

function HamburgerException(message) {
    this.message = message;
};

HamburgerException.prototype = Object.create(Error.prototype);

function Hamburger(size, stuffing) {
    this.TOPPINGS = [];
    if(!size && !stuffing){
        throw new HamburgerException('Size and stuffing are not given!');
    }
    if(size != Hamburger.SIZE_LARGE && size != Hamburger.SIZE_SMALL){
        throw new HamburgerException('Invalid hamburger size ' + size);
    }
    if(stuffing != Hamburger.STUFFING_CHEESE && stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POTATO){
        throw new HamburgerException('Invalid hamburger stuffing ' + stuffing);
    }else{
        this._size = size;
        this._stuffing = stuffing;
    }
};

Hamburger.prototype.addTopping = function(topping){
    if(topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE){
        throw new HamburgerException('Invalid name of the topping!')
    }
    for(key of this.TOPPINGS) {
        if (key.type == topping.type) {
            throw new HamburgerException('This type of topping is already added!');
        }
    }
    console.log(topping.type + ' was added!')
    return this.TOPPINGS.push(Object.assign({}, topping));
};

Hamburger.prototype.removeTopping = function (topping){
    if(topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE){
        throw new HamburgerException('Invalid name of the topping!')
    }
    for (key of this.TOPPINGS) {
        if (key.type == topping.type) {
            this.TOPPINGS.splice(this.TOPPINGS.indexOf(key), 1);
            console.log(topping.type + ' was removed!')
        }
        else if (this.TOPPINGS.indexOf(key) == this.TOPPINGS.length - 1) {
            throw new HamburgerException('This type of topping was not added!');
        }
    }


};

Hamburger.prototype.getToppings = function (){
    return this.TOPPINGS;
};

Hamburger.prototype.getSize = function (){
    return this._size;
};

Hamburger.prototype.getStuffing = function (){
    this._stuffing;
}

Hamburger.prototype.calculatePrice = function (){
    let toppingPrice = 0;
    for(let i=0;i<this.TOPPINGS.length;i++){
        toppingPrice+=this.TOPPINGS[i].price;
    }
    return this._size.price + this._stuffing.price + toppingPrice;
}

Hamburger.prototype.calculateCalories = function (){
    let toppingCalories = 0;
    for(let i=0;i<this.TOPPINGS.length;i++){
        toppingCalories+=this.TOPPINGS[i].calories;
    }
    return this._size.calories + this._stuffing.calories + toppingCalories;
};

// маленький гамбургер с начинкой из сыра

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);

// // добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
// // // А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
console.log("Is hamburger small: %s", hamburger.getSize() === Hamburger.SIZE_SMALL); // -> true
// // Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
//
// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);